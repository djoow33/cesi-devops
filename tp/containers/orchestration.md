# Orchestration de conteneurs

Lorsque l'on souhaite utiliser des conteneurs en production, il peut être intéressant de profiter de fonctionnalités additionnelles comme de la haute-disponibilité et la mise en cluster des hôtes de conteneurisation.

Afin de construire un cluster d'hôtes de conteneurisation, deux principales solutions se présentent (non exhaustif) :
* Kubernetes : complexe, parfois un peu lourd, mais très robuste et performant
* Docker Swarm : natif Docker, simple d'utilisation et mise en place, idéal pour appréhender l'orchestration

Les infrastructures modernes, orientées sur les services fournis, essaient de mettre en valeur les caractéristiques suivantes :
* Infrastructure as Code et automatisation
  * avec des outils comme Vagrant ou Terraform par exemple pour la création des machines
  * et Docker, docker-compose, et autres Ansible/Puppet pour le déploiement des services
  * cela nous amène une **répétabilité des déploiements**, ainsi qu'une **conformité** et une **hausse de qualité et de sécurité**
  * utilisation de `git`
* écosystème riche
  * il existe désormais de nombreuses solutions intégrées à ces environnements
  * chaque fonction est remplie par une application dédiée
  * monitoring, récolte de logs, proxying, etc.
* déploiements facilités, aisés
  * déploiements simples, rapides et scalables
  * méthode de packaging unifiée (conteneurs, Infrastructure as Code)
* robustesse
  * stockage redondé
  * applications hautement disponibles

---

**Le but de ce TP** :
* construire un cluster Docker Swarm
* mettre en évidence les fonctionnalités de HA
* mettre en place un registry maison
* mettre en place des applications à travers le swarm

## Sommaire

<!-- vim-markdown-toc GitLab -->

* [I. Docker Swarm intro](#i-docker-swarm-intro)
    * [Mise en place du Swarm](#mise-en-place-du-swarm)
    * [Vocabulaire Swarm](#vocabulaire-swarm)
* [II. Déployer une première application](#ii-déployer-une-première-application)
* [III. Mettre en place un registre Docker](#iii-mettre-en-place-un-registre-docker)
    * [Mise en place du registre](#mise-en-place-du-registre)
    * [Utiliser le registre](#utiliser-le-registre)
* [IV. Lancer une application plus complexe](#iv-lancer-une-application-plus-complexe)
* [V. Services adapatés à Swarm](#v-services-adapatés-à-swarm)
    * [Reverse-proxy dyamique : Traefik](#reverse-proxy-dyamique-traefik)
    * [Stockage redondé : MinIO](#stockage-redondé-minio)
    * [Monitoring : Stack Prometheus](#monitoring-stack-prometheus)

<!-- vim-markdown-toc -->

# I. Docker Swarm intro

## Mise en place du Swarm

On va construire un cluster de 3 noeuds : créez 3 machines virtuelles CentOS7 (512Mo RAM sont suffisants, 1Go c'est cool pour être tranquille).

Pour faire cela, je vous conseille :
* construction d'une box Vagrant avec Docker installé et configuré
  * base CentOS7
  * installation de Docker
  * installation de `docker-compose`
  * ajout de l'utilisateur `vagrant` au groupe `docker`
  * activation du service `docker` au démarrage de la machine
* création d'un `Vagrantfile`
  * 3 VMs sont déclarées
  * elles ont une IP locale
  * elles utilisent la box Docker construite
* **`vagrant up` !**

---

Une fois les 3 machines en place, vous pouvez initialiser le cluster :
```bash
# Sur un premier noeud
docker swarm init --advertise-addr <LAN_IP>

# Obtenir un token pour ajouter des workers au cluster
docker swarm join-token worker
# Obtenir un token pour ajouter des managers au cluster
docker swarm join-token manager

# Sur les autres noeuds
docker swarm join ... # réutiliser la commande obtenue avec le 'join-token'
```

## Vocabulaire Swarm

Un peu de vocabulaire Swarm :
* *node*
  * un *node* est un membre du cluster Swarm
    * un *worker* est chargé de lancer des conteneurs
    * un *manager* peut lancer des conteneurs et participent à la bonne santé du cluster
* *stack*
  * une *stack* est un `docker-compose.yml` lancée dans un Swarm
* *service*
  * une *stack* est composée de *services*
  * un *service* est composé d'un ou plusieurs conteneurs basé(s) sur une image donnée
  * c'est pour ça que vous écrivez "services" dans le `docker-compose.yml` :)

Les commandes élémentaire pour manipuler le Swarm :
```bash
# Liste les noeuds du cluster
$ docker node ls

# Liste les stacks
$ docker stack ls

# Liste les services d'une stack
$ docker stack services <STACK_NAME>

# Récupère les logs d'un service
$ docker service logs <SERVICE_NAME>

# Récupère des infos sur un service
$ docker service ps <SERVICE_NAME>
```

# II. Déployer une première application

Pour déployer une application dans un Swarm, elle doit être sous forme de `docker-compose.yml`.

**Créez un `docker-compose.yml` simple qui ne contient qu'une unique conteneur.** Ce conteneur doit lancer le serveur web NGINX au démarrage.

**Déployez le `docker-compose.yml` dans le Swarm :**
```bash
$ ls
docker-compose.yml

# Déploiement dans le Swarm
$ docker stack deploy -c docker-compose.yml test-stack
```

**Vérifiez le bon fonctionnement**

* testez que l'application fonctionne en visitant le serveur web.
* jouez avec le cluster
  * utilisez avec la commande `docker service scale` afin d'augmenter le nombre de serveurs Web.
  * détruisez un noeud pour vérifier la haute-disponibilité
    * observez la bascule automatique des conteneurs

# III. Mettre en place un registre Docker

## Mise en place du registre

Un **registre d'image** Docker est une application permettant de **stocker et distribuer des images** Docker. Le Docker Hub est un registre Docker public.

> Afin de simplifier le déploiement du registre, il sera déployé comme un simple docker-compose (PAS dans le Swarm).

**Déployer un registre Docker simple**

* déployez sur `node1` avec une commande `docker-compose up`
  * utilisez [le `docker-compose.yml` fourni](./registry/docker-compose.yml)
  * il faudra créer les trois répertoires renseignés à l'intérieur
* configurer les fichier hosts de vos noeuds pour qu'ils puissent joindre le registre sur `registry.cesi` (IP de node1 qui pointe vers `registry.cesi`)
* modifier la configuration de votre démon docker pour ajouter l'option `insecure-registries`
  * car on utilise pas de HTTPS ici
  * pour ce faire :

```bash
# Modification du fichier de configuration de Docker pour qu'il soit conforme à :
$ cat /etc/docker/daemon.json
{
  "insecure-registries" : ["registry.cesi:5000"]
}

# Redémarrer le démon Docker
$ sudo systemctl restart docker
```

## Utiliser le registre

**Vous pouvez alors pousser** (`docker push`) et **récupérer** (`docker pull`) des images vers/depuis votre registre :

```bash
# Récupération d'une image extérieure
docker pull alpine

# Renommage
docker tag <NAME> <REGISTRY>/<REPO_NAME>/<IMAGE_NAME>:<TAG>
docker tag alpine registry.cesi:5000/alpine/alpine:test

# Push
docker push registry.cesi:5000/alpine/alpine:test

# Récupération 
docker pull registry.cesi:5000/alpine/alpine:test

# Et/ou utilisation
docker run -it registry.cesi:5000/alpine/alpine:test sh
```

---

**Poussez sur le registre l'image Python créée dans le [TP d'inititiation à Docker](./first_steps.md#création-dun-docker-composeyml).**

* vous devrez la tagger correctement pour qu'elle puisse être push sur le registre

# IV. Lancer une application plus complexe

On va réutiliser le `docker-compose.yml` construit dans [le TP d'initiation à la conteneurisation](./first_steps.md#création-dun-docker-composeyml).

**Déployez l'application Python + le Redis dans Swarm**

* récupérez votre `docker-compose.yml`
* changez l'image utilisée par l'app Python pour être l'image du registre (avec son nom complet)
* effectuez un partage de port pour pouvoir joindre l'application

> Un partage de ports dans un Swarm a pour effet que TOUS les noeuds du cluster exposeront et partageront le port vers le bon conteneur.

---

**Tester le bon fonctionnement**

* visitez l'application Web 
  * grâce au partage de ports, TOUS les noeuds du cluster expose le service sur le port partagé
* faire scale l'application Web à 3 instances et vérifier sur l'interface (le nom du conteneur qui traite la requête est affichée sur l'app)

# V. Services adapatés à Swarm

## Reverse-proxy dyamique : Traefik

Actuellement, un service web ne peut être exposé que directement en ouvrant un port sur tous les noeuds du cluster.  

Dans cette partie, vous mettre en place [Traefik](https://docs.traefik.io/) afin de gérer les accès aux services Web qui tournent dans le cluster. 

> Coupez la stack Python lancée dans la précédente partie pour la suite. 

**Traefik est un reverse proxy** dynamique, qui s'intègre nativement avec Swarm. Il est **capable de détecter des applications** qui sont créées dans le Swarm, et de modifier automatiquement sa configuration afin de les servir. 

---

**Créer un réseau dédié à Traefik**  

* il sera utilisé pour Traefik
* toutes les applications qui auront besoin du reverse proxy Traefik seront aussi dans ce réseau

```
# Création du réseau
docker network create --driver overlay traefik

# Vérifier la création. Le réseau doit être présent sur tous les noeuds du cluster
docker network ls
```

**Déployer une *stack* Traefik**
* utiliser [le `docker-compose.yml` fourni](./traefik/docker-compose.yml)
  * modifier le `docker-compose.yml` pour partager le port 8080
    * c'est à dire, ajoutez un `8080:8080` en plus de ce qu'il y a déjà
  * noter l'utilisation du réseau `traefik` en `external` 
    * cela indique qu'il doit être créé auparavant
    * ce réseau sera aussi utilisé pour les prochaines applications que l'on mettra derrière le reverse proxy
* vérifier le bon fonctionnement en visitant l'interface web de Traefik
  * grâce au partage de port
  * `http://<IP_VM>:8080/dashboard/`

---

**Passer l'application Web Python derrière Traefik**
* réutiliser encore le `docker.compose.yml` de l'app Python
  * positionner des labels dans le `docker-compose.yml` afin que Traefik détecte l'application
  * **enlever le partage de ports** que vous aviez mis
  * préciser l'utilisation du réseau `traefik` en `external` (référez-vous au [`docker-compose.yml` de traefik](./traefik/docker-compose.yml) pour la syntaxe)
* l'application sera accessible *via* un nom de domaine 
  * le `<DOMAIN_NAME>` choisi dans l'exemple ci-dessous
  * sans serveur DNS, **il faudra ajouter le nom à votre fichier hosts**
* tester
  * avec un `curl` ou un navigateur web

Labels à positionner dans le `docker-compose.yml` : 
```
# Ajout de la clause deploy, dans un service
services:
  example:
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.<SERVICE_NAME>.rule=Host(`<DOMAIN_NAME>`)"   # ce sont des backticks, pas des simple quotes
        - "traefik.http.routers.<SERVICE_NAME>.entrypoints=web"
        - "traefik.http.services.<SERVICE_NAME>.loadbalancer.server.port=<SERVICE_PORT>"

# Exemple
services:
  python-webapp:
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.python-webapp.rule=Host(`python-webapp`)"   # ce sont des backticks, pas des simple quotes
        - "traefik.http.routers.python-webapp.entrypoints=web"
        - "traefik.http.services.python-webapp.loadbalancer.server.port=8888"
```

> Attention dans "Host(`python-webapp`)" ce sont des **backticks**, pas des simples quotes.

**Mettre le dashboard de Traefik derrière le proxy**
* pour ce faire, modifier le `docker-compose.yml` de Traefik
  * ajouter des labels sur le conteneur Traefik
* relancer la stack Traefik

## Stockage redondé : MinIO

> Note : Lisez en entier avant de vous lancer.

**S3 est devenu *de facto* un standard pour le stockage** dans les environnements avec de l'orchestration de conteneurs ou, plus largement, dans les environnements Cloud.  
S3 doit son nom à l'offre de services [Amazon S3](https://aws.amazon.com/fr/s3/), qui propose du stockage *via* une API HTTP.  

L'API étant très utilisée, elle est devenue un outil très commun au sein des environnements. Ainsi, d'autres solutions ont vu le jour, d'autres solutions qui implémente cette même API S3.  

C'est le cas de [Minio](https://min.io/) que nous allons mettre en place. 

---

Minio sera lancé comme une stack dans le cluster Swarm.  
Minio est une solution de stockage, on doit lui fournir un endroit où stocker ses données. 

| Conteneur | Hôte       | Data Path  |
|-----------|------------|------------|
| `minio1`  | `node1.b3` | `/minio`   |
| `minio2`  | `node2.b3` | `/minio`   |
| `minio3`  | `node3.b3` | `/minio`   |
| `minio4`  | `node3.b3` | `/minio-2` |

**Préparer l'environnement**
* créez les dossiers de données Minio sur tous les noeuds
* comme indiqué dans le tableau plus haut, il faut créer 4 dossiers (en effet MinIO réplique au minimum 4 fois les données)

> **Afin de ne pas perturber l'instance Minio** à travers les éventuels redémarrages (reboot machine, redémarrage docker, etc), **on va faire en sorte que les conteneurs Minio se lancent respectivement toujours au même endroit** grâce à des labels Swarm. See below.

---

**Déployer Minio**
* [déployer Minio en utilisant le `docker-compose.yml` de la doc](https://docs.min.io/docs/deploy-minio-on-docker-swarm)
  * pour la partie avec les labels de noeuds, nous, on a que trois noeuds, donc :

```
docker node update --label-add minio1=true <DOCKER-NODE1>
docker node update --label-add minio2=true <DOCKER-NODE2>

# Puis deux fois le node3
docker node update --label-add minio3=true <DOCKER-NODE3>
docker node update --label-add minio4=true <DOCKER-NODE3>
```

**Déployer Minio** (suite)
* dans le `docker-compose.yml`
  * ne pas exposer Minio avec un partage de port
    * enlever le partage de ports du `docker-compose.yml`
  * exposer Minio à travers Traefik 
    * ajouter des labels dans le `docker-compose.yml`
    * il faut aussi ajouter les conteneurs minio dans le réseau créé pour Traefik, pour que ce dernier puisse les joindre
    * Minio expose son interface Web sur le port 9000/TCP
* créer un bucket sur l'interface Web afin de tester le bon fonctionnement

---

**Tester Minio**
* accéder à l'interface Web et tester la création de Bucket, l'upload de fichiers
* utiliser [s3fs](https://github.com/s3fs-fuse/s3fs-fuse) pour monter votre bucket comme une partition
* créer un fichier de test, vérifier sa création sur l'interface Web (ou avec [un client Minio](https://docs.min.io/docs/minio-client-complete-guide))

> Dans le cas d'une utilisation réelle, Minio peut être positionné sur des serveurs de stockage afin d'en maximiser les performances. 

## Monitoring : Stack Prometheus

**Prometheus est un outil utilisé pour analyser des données et métriques**. On peut ainsi s'en servir pour monitorer un parc de machines. Il est **particulièrement adapté aux environnements cloud** (c'est un projet soutenu par la CNCF).

Prometheus n'est que l'entité qui stocke et permet d'effectuer des requêtes sur des données. **Pour l'exploiter pleinement, on ajoute souvent** : 
* **des récolteurs de données**
  * installés sur les noeuds, ils récoltent les différentes métriques de l'hôte
  * ils exportent les données vers Prometheus
* un outil qui permet de **visualiser les données**
  * afin de mieux exploiter les données de Prometheus, il peut être intéressant de créer des dashboards pour visualiser ces données

---

Pour que le setup soit plus rapide, il existe déjà des travaux qui ont été faits pour lier une stack avec Prometeus à un Swarm Docker. On va ici utiliser [swarmprom](https://github.com/stefanprodan/swarmprom).

**Déployer Swarmprom**
* expliquer l'utilité des différents composants de swarmprom (autres que Prometheus)
* mettre le dashboard tous les services web derrière Traefik
  * il y a déjà un `docker-compose.traefik.yml` dans le dépôt swarmprom
  * adaptez le en fonction de ce qu'on a fait jusqu'à maintenant
  * c'est à dire, remplacer les labels Traefik par les nôtres !
* aucun partage de ports ne doit être utilisé (uniquement le reverse proxy Traefik)
* vous pouvez utiliser les alertes Slack si vous avez un channel (ça se crée vite fait avec un compte sinon :) ) mais rien d'obligatoire !

> Le principal Dashboard est disponible sur Grafana !
