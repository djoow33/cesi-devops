#!/bin/bash

# Add EPEL repos
yum install -y epel-release 

# Update system
yum update -y

# Install some additional packages
yum install -y python3 vim

# Start VBox guest additions
systemctl enable vboxadd
systemctl enable vboxadd-service

# Clean caches and artifacts
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
